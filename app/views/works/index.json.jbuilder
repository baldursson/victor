json.array! @works do |work|
  json.href work.image.big.url
  json.type 'image/jpeg'
  json.thumbnail work.image.thumbnail.url
end