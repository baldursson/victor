# encoding: utf-8

class ArtUploader < CarrierWave::Uploader::Base

  include Cloudinary::CarrierWave

  version :big do
    resize_to_fit(1200, 1200)
  end

  version :retina_big do
    resize_to_fit(2400, 2400)

    def full_filename(for_file = model.avatar.file)
      super.tap do |file_name|
        file_name.gsub!('.', '@2x.').gsub!('retina_', '')
      end
    end
  end

  version :thumbnail do
    resize_to_fit(50, 50)
  end

  version :retina_thumbnail do
    resize_to_fit(100, 100)

    def full_filename(for_file = model.avatar.file)
      super.tap do |file_name|
        file_name.gsub!('.', '@2x.').gsub!('retina_', '')
      end
    end
  end

end
