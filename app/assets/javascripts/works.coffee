$ ->
  initCarousel()

initCarousel = ->
  $.getJSON '/works.json', (data) ->
    gallery = blueimp.Gallery data,
      container: "#blueimp-gallery-carousel"
      carousel: true
      fullScreen: true
      enableKeyboardNavigation: true

    $('#works').on 'click touchstart', ->
      gallery.pause()

    $(document).keydown (e) ->
      switch e.keyCode
        when 37, 39 then gallery.pause()