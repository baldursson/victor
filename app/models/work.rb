# == Schema Information
#
# Table name: works
#
#  id          :integer          not null, primary key
#  title       :string
#  image       :string
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Work < ActiveRecord::Base
  mount_uploader :image, ArtUploader
end
