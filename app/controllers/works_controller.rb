class WorksController < ApplicationController
  def index
    @works = Work.order(created_at: :desc)
    respond_to do |format|
      format.html
      format.json 
    end
  end
end
