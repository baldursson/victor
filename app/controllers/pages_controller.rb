class PagesController < ApplicationController
  before_action :set_page, only: [:show]

  respond_to :html

  def show
    respond_with(@page)
  end

  private
    def set_page
      @page = Page.friendly.find(params[:id])
    end
end
