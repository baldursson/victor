# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create_with(password: '12345678').find_or_create_by!(email: 'victor@victorrankanen.se')

Page.create_with(body: 'Im a painter and a drawer from Gothenburg, Sweden.
I mostly work abstract with patterns and shapes. Im trying to find the best way to make use of an empty flat surface. Please contact me for questions or requests. <a href="mailto:art@victorrankanen.com">art@victorrankanen.com.</a>').find_or_create_by!(title: 'About')
